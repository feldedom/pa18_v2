#ifndef valueStruct_h
#define valueStruct_h


struct value{
    float lambdaSet;
    float lambdaIn;
    float lambda_level;
    float lambda_offset;

    float rpmSet;
    float rpmIn;
    float rpm_level;
    float rpm_offset;
} typedef value;

struct tv{
    float mix;
    float air;
} typedef tv;

#endif