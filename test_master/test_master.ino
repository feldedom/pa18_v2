// Wire Master Writer
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Writes data to an I2C/TWI slave device
// Refer to the "Wire Slave Receiver" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>
#include "valueStruct.h"
#include "com.h"

byte x = 0;

value data;
tv throttlevalve;
value simulation(value);

char bytes2send[16];
char bytes2recive[8];

void setup() {
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(9600);

  data.lambdaSet = 1;
  data.lambdaIn = 1;
  data.lambda_level = 0.2;
  data.lambda_offset = 0.001;
  
  
  data.rpmSet = 3000;
  data.rpmIn = 3000;
  data.rpm_level = 100;
  data.rpm_offset = 0.1;
}


void loop() {

  data = simulation(data);
  ConvertValuestoArrayM(data,bytes2send);

  Serial.println(data.rpmIn);
  Serial.println(data.lambdaIn);

  Wire.beginTransmission(12); // transmit to device #8
  
  for(int i = 0; i<sizeof(bytes2send); i++){
    Wire.write(bytes2send[i]);
  }
  Serial.println("send");

  delay(1000);

  
  Wire.requestFrom(12,8);

  int i = 0;
  while (Wire.available()) { // slave may send less than requested
    bytes2recive[i] = Wire.read(); // receive a byte as character
    i++;
  }
  throttlevalve = ConvertArraytoValuesM(bytes2recive);


  Serial.println(throttlevalve.air);
  Serial.println(throttlevalve.mix);

  Wire.endTransmission();    // stop transmitting
  delay(1000);
  
}


value simulation(value r){
  static bool dirRPM = 1;

  if(dirRPM == 1){
    r.rpmIn += r.rpm_offset;
  } else r.rpmIn -= r.rpm_offset;
  
  if(r.rpmIn > r.rpmSet + r.rpm_level){
    dirRPM = 0;
  } else if(r.rpmIn < r.rpmSet - r.rpm_level){
    dirRPM = 1;
  }

  static bool dirLambda = 1;

  if(dirLambda == 1){
    r.lambdaIn += r.lambda_offset;
  } else r.lambdaIn -= r.lambdaIn;
  
  if(r.lambdaIn > r.lambdaSet + r.lambda_level){
    dirLambda = 0;
  } else if(r.lambdaIn < r.lambdaSet - r.lambda_level){
    dirLambda = 1;
  }

  return r;
}
