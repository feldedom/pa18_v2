/* header file with communication functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       11.10.2018
version:    2.0
*/

#ifndef com_h
#define com_h

#include <Arduino.h>
#include <Wire.h>
#include "valueStruct.h"


tv ConvertArraytoValuesM(char*);

void ConvertValuestoArrayM(value, char*);

unsigned long castingM(float);

float decastingM(unsigned long);


#endif
