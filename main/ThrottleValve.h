/* header file for a throttle valve object for the engine controllerauthor:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#ifndef ThrottleValve_h
#define ThrottleValve_h

#include "PID_v1.h"
#include <Arduino.h>
#include "definitions.h"

class ThrottleValve
{
    public:

        int force;                           //limit for the analog output
        double TPS;                          //for maping
        int Ref_max;                         //zero position
        int Ref_min;                         //max position
        double tolerance;                    //Tolerance for 
        double Setpoint, Input, Output;

        int  adr_analog_input;            
        int  adr_analog_output;
        
        PID controller;
        ThrottleValve(double, double, double, int, int, int, int, int, double);        // * constructor.  links the PID to the Input, Output, and 
    //Constructor
        
        void setTVposition(float);
        float getTVposition(void);

};


#endif
