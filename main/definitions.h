#ifndef definitions_h
#define definitions_h


// DEFINE IN and OUTPUTS
#define TVM_INPUT               A0     //analog input adress throttle valve mix 
#define MOTOR_TVM_OUTPUT        5      //analog output adress for throttle valve motor air (PWM)

#define TVA_INPUT               A2     //analog input adress throttle valve air
#define MOTOR_TVA_OUTPUT        6      //analog output adress for throttle valve motor air (PWM)

#define POTI1_INPUT             A3     //analog input adress pot1
#define POTI2_INPUT             A1     //analog input adress pot2

#define RANGE                   100    //Range for scale

#define MODE_INPUT              4      //digital input for mode on pin 4
#define EMERGENCY_STOP_PIN      2      //digital input for emergencyStop on pin2

#define SAMPLE_TIME_PID_TV      10     //sample time for pid throttle valve controller
#define SAMPLE_TIME_PID         50     //sample time for pid lambda and rpm controller


//struct for data
struct value{
    float lambdaSet;
    float lambdaIn;
    float rpmSet;
    float rpmIn;
} typedef value;


//enum for state machine
enum states {manual,automatic};

#endif