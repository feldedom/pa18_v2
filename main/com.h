/* header file with communication functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       11.10.2018
version:    2.0
*/

#ifndef com_h
#define com_h

#include <Arduino.h>
#include <Wire.h>
#include "definitions.h"


value ConvertArraytoValues(char*);

void ConvertValuestoArray(float, float, char*);

unsigned long casting (float);

float decasting (unsigned long);


#endif
