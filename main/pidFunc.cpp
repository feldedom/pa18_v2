/* cpp file with functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       16.10.2018
version:    2.0
*/

#include "pidFunc.h"



// Variables for Lambda controller
double KpL=4, KiL=0.00, KdL=0.00; // only KdL 
double SetpointL, InputL, OutputL;
PID* getPosLambda = new PID(&InputL, &OutputL, &SetpointL, KpL, KiL, KdL, DIRECT); 

float lambdaMin = 0.68;
float lambdaMax = 1.36;

// Variables for RPM controller
double KpR=4, KiR=0.00, KdR=0.00; // only KdL 
double SetpointR, InputR, OutputR;
PID* getPosRPM = new PID(&InputR, &OutputR, &SetpointR, KpR, KiR, KdR, DIRECT); 
float rpmMin = 0;
float rpmMax = 7200;



// Functions
//-----------------------------------------------

//controller for lambda and rpm
//returns set value for valve_position
void initController(void){
    //turn the PID on
    getPosLambda->SetMode(AUTOMATIC);
    getPosRPM->SetMode(AUTOMATIC);
  
    //  pinMode(RPM_Pin,INPUT_PULLUP);
    getPosLambda->SetSampleTime(SAMPLE_TIME_PID);
    getPosRPM->SetSampleTime(SAMPLE_TIME_PID);
    //--------------------------------------------------

}


float controllerLambda(float set, float input){

    double inTPS = (double) map(input,lambdaMin,lambdaMax,0,RANGE);

    double setTPS = (double) map(set,lambdaMax,lambdaMax,0,RANGE);

    InputL      = inTPS;
    SetpointL   = setTPS;
    
    getPosLambda->Compute();

    return (float) OutputL;
}

float controllerRpm(float set, float input){

    double inTPS = (double) map(input,rpmMin,rpmMax,0,RANGE);
    double setTPS = (double) map(set,rpmMin,rpmMax,0,RANGE);
  
    InputR      = inTPS;
    SetpointR   = setTPS;
    
    getPosRPM->Compute();

    return (float) OutputR;
}
