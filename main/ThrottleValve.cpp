/* cpp file for a throttle valve object for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#include "ThrottleValve.h"




//setup for ThrottleValve
ThrottleValve::ThrottleValve(double kp, double ki, double kd, int POn, int ControllerDirection, int adr_analogInput, int adr_analogOutput, int force,double tolerance)
        :controller(&Input,&Output,&Setpoint,kp,ki,kd,POn,ControllerDirection){
        
    adr_analog_input = adr_analogInput;
    adr_analog_output = adr_analogOutput;

    /*Debug*/    
    //Serial.println("new ThrottleValve");
}

//sets ThrottleValve positon
void ThrottleValve::setTVposition(float tv_position){
    Setpoint = tv_position;

    //Istwert wird eingetragen und an Regelstrecke übertragen
    //Referenzwert 0 wird vom TPS Signal von Anfang an abgezogen!
    TPS = analogRead(adr_analog_input);

    TPS = map(TPS,Ref_min,Ref_max,0,RANGE);
    Input = TPS;

    //Regler wird aufgerufen
    controller.Compute();
    //Output von Regler wird in einen Bereich eingeschränkt
    //Wenn Bereich vergrössert wird ist die Klappe schneller aber überschwingt eher
    Output = constrain(Output,0,force);
    //PWM Signal wird auf Motor1_Output übergeben
    analogWrite(adr_analog_output, Output);
}

//gets ThrottleValve positon
float ThrottleValve::getTVposition(void){

        float ret_val = map(analogRead(adr_analog_input),0,1023,0,RANGE);

        return ret_val;   
}
