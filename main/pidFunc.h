/* header file with functions for the engine controller
author:     Dominik Felder, Balz Huber
date:      16.10.2018
version:    2.0
*/

#ifndef pidFunc_h
#define pidFunc_h

#include "PID_v1.h"
#include <Arduino.h>
#include "definitions.h"

//controller for lambda and rpm
//returns delta of valve_position in percent

void initController(void);
float controllerLambda(float set, float input);
float controllerRpm(float set, float input);



#endif