/*FINAL STATE MACHINE for controlling the engine of the woodgasifier
desgined for an arduino uno

author:     Dominik Felder, Balz Huber
date:       11.10.2018
version:    2.0

*/


// INCLUDE
#include "com.h"
#include "PID_v1.h"
#include "ThrottleValve.h"
#include "pidFunc.h"
#include "Poti.h"


//variables
//---------------------------------------------
int state = 0;

char BytesReceived[17];             // Store received Bytes
char BytesToSend[8];                //Prepare Bytes to send 

value Parameters;

float TVAist = 0.5555;
float TVMist = 0.5555;
//---------------------------------------------


// Create ThrottleValve Objects TVA and TVM
//---------------------------------------------
// ThrottleValve Mix
double Kp_tvm=3.5, Ki_tvm=0.5, Kd_tvm=0;
int range_tvm = 100, force_tvm = 70;
double toleranceM = 10;
ThrottleValve* TVM = new ThrottleValve(Kp_tvm,Ki_tvm,Kd_tvm,0,DIRECT,TVM_INPUT,MOTOR_TVM_OUTPUT,force_tvm,toleranceM);

// ThrottleValve Air
double Kp_tva=4, Ki_tva=0.1, Kd_tva=0;
int range_tva = 100, force_tva = 65;
double toleranceA = 10;
ThrottleValve* TVA = new ThrottleValve(Kp_tva,Ki_tva,Kd_tva,0,DIRECT,TVA_INPUT,MOTOR_TVA_OUTPUT,force_tva,toleranceA);
//---------------------------------------------


// Create Poti Objects PotiM and PotiA
//---------------------------------------------
Poti PotiA(POTI1_INPUT);
Poti PotiM(POTI2_INPUT);
//---------------------------------------------



void setup(){

    // Initialize Parameters
    //--------------------------------------------------
    Parameters.lambdaIn = 0;
    Parameters.lambdaSet = 1.05;
    Parameters.rpmIn = 0;
    Parameters.rpmSet = 3000;
    //--------------------------------------------------
    
    
    //Setup I2C Communication with Master(MEGA)
    //--------------------------------------------------
    for (int i=0;i<17;i++){
        BytesReceived[i]=0;
      }
    for (int i=0;i<8;i++){
        BytesToSend[i]='A';
      }

    Wire.begin(12);                // join i2c bus with address #12
    Wire.onReceive(receiveEvent); // register event 
    Wire.onRequest(requestEvent); // 
    Serial.begin(9600);           // start serial for output
    //--------------------------------------------------
    
    // set timer 0 divisor to 1 for PWM frequency of 31372.55 Hz
    TCCR0B = TCCR0B & B11111000 | B00000001;    //sets PWM to 32 KHz you really need this

    //Setup Emergency Stop Interrupt
    //--------------------------------------------------
    pinMode(EMERGENCY_STOP_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(EMERGENCY_STOP_PIN), ISRemergencyStop, LOW);
    //--------------------------------------------------


    //Setup for Throttle Valve PID Objects
    //--------------------------------------------------
    TVA->Ref_min = analogRead(TVA_INPUT);
    TVM->Ref_min = analogRead(TVM_INPUT);
    
    analogWrite(MOTOR_TVA_OUTPUT,TVA->force);
    analogWrite(MOTOR_TVM_OUTPUT,TVM->force);
    
    delay(10000);

    TVA->Ref_max = analogRead(TVA_INPUT);
    TVA->Ref_max*=0.85;
    analogWrite(MOTOR_TVA_OUTPUT,0);

    TVM->Ref_max = analogRead(TVM_INPUT);
    TVM->Ref_max*=0.85;
    analogWrite(MOTOR_TVM_OUTPUT,0);

    //turn the PID on
    TVA->controller.SetMode(AUTOMATIC);
    TVM->controller.SetMode(AUTOMATIC);
  
    //  pinMode(RPM_Pin,INPUT_PULLUP);
    TVA->controller.SetSampleTime(SAMPLE_TIME_PID_TV);
    TVM->controller.SetSampleTime(SAMPLE_TIME_PID_TV);
    //--------------------------------------------------

    // Setup the PID Controller for Lambda and RPM
    //--------------------------------------------------
    initController();
    //--------------------------------------------------

}


void loop(){
/*
    states:

    state 0 -> manuel mode
    state 1 -> automatic mode
    default -> manual mode
    */
   state = digitalRead(MODE_INPUT);

    switch (state)
    {
        case manual:
            
            TVA->setTVposition(PotiA.getPoti());
            TVM->setTVposition(PotiM.getPoti());

            // preparing data transfer to mega
            TVAist = TVA->getTVposition();
            TVMist = TVM->getTVposition();
            

            break;
        case automatic: 

            // Automatic mode


            // reading the valve positions
            TVAist = TVA->getTVposition();
            TVMist = TVM->getTVposition();

            // As long as the TVA don't enter the Tolerance it won't get a new Setpoint    
            if(abs(TVA->Setpoint-(double)TVAist) <= TVA->tolerance){
                TVA->setTVposition(controllerLambda(Parameters.lambdaSet,Parameters.lambdaIn)+TVA->Setpoint);
            } else {
                TVA->setTVposition(TVA->Setpoint);
                }
            
            
            // As long as the TVM don't enter the Tolerance it won't get a new Setpoint  
            if(abs(TVM->Setpoint-(double)TVMist) <= TVM->tolerance){
                TVM->setTVposition(controllerLambda(Parameters.rpmSet,Parameters.rpmIn)+TVM->Setpoint);
            } else {
                TVM->setTVposition(TVM->Setpoint);
                }

    

            break;
    
        default:

            state = manual; // default state -> manual mode
            
            break;
    }


    delay(100);
}


// Interrupt Service Routine for Emergency Stop
void ISRemergencyStop(){

    // closes both Throttle Valves
    TVA->setTVposition(0);
    TVM->setTVposition(0);

}



// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
    int i=0;
    while (1 < Wire.available()) { // loop through all but the last
        BytesReceived[i] = Wire.read(); // receive byte as a character
        i++;

    }


    
    
    Parameters = ConvertArraytoValues(BytesReceived);
    Serial.println("recveive");
    Serial.println(Parameters.rpmIn);
    Serial.println(Parameters.rpmSet);
    Serial.println(Parameters.lambdaIn);
    Serial.println(Parameters.lambdaSet);

    int x = Wire.read();
    Serial.println(x);

    

}



// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Serial.print("request\n");
//   for(int i = 0; i<8;i++){
//     Serial.println(BytesToSend[i]);
//   }   
  
ConvertValuestoArray(TVAist,TVMist,BytesToSend);
  
// for(int i = 0; i<8;i++){
//     Serial.println(BytesToSend[i]);
// }

  Wire.write(BytesToSend, 8); // respond with message of 6 bytes
  // as expected by master
  delay(100);

  
}
