/* cpp file with communication functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       11.10.2018
version:    2.0
*/


#include "com.h"

//variables

int mode = 0;


//functions
value ConvertArraytoValues(char BytesReceived[]){
  value Data;
  int b=0;
  unsigned long a=0;
  for (int i=b;i<4;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Data.lambdaIn=decasting(a);
  
  b=4;
  a=0;
  for (int i=b;i<8;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Data.lambdaSet=decasting(a);
  
  b=8;
  a=0;
  for (int i=b;i<12;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Data.rpmIn=decasting(a);
  
  b=12;
  a=0;
  for (int i=b;i<16;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Data.rpmSet=decasting(a);

  if (BytesReceived[16]<1){
    mode=0;
    }
    else{
      mode=1;
    }

    return Data;

}

void ConvertValuestoArray(float TVAist, float TVMist, char BytesToSend[]){
  unsigned long b=casting(TVAist);
  for (int i=0;i<4;i++){
    BytesToSend[i]=(b>>(24-i*8))&0xFF;
  }
  b=casting(TVMist);
  for (int i=4;i<8;i++){
    BytesToSend[i]=(b>>(24-(i-4)*8))&0xFF;
  }
}


unsigned long casting (float a){
  unsigned long longo=a*100;
  return(longo);
}


float decasting (unsigned long a){
  float floato=((float) a)/100;
  return(floato);
}
